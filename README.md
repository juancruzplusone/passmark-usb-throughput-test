# Passmark USB Throughput Test

Modification of Passmark API example program. Prints average throughput of a port where USB3.0 Loopback tester connected.